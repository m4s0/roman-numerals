__author__ = 'm4s0'

import sys

SYMBOLS = [
    ('I', 1),
    ('IV', 4),
    ('V', 5),
    ('IX', 9),
    ('X', 10),
    ('XL', 40),
    ('L', 50),
    ('XC', 90),
    ('C', 100),
    ('CD', 400),
    ('D', 500),
    ('CM', 900),
    ('M', 1000),
    ('MMMM', 4000),
    ('v', 5000)
]


def convertRoman(n):
    r = ''
    while n > 0:
        for l, v in SYMBOLS[::-1]:
            if n - v >= 0:
                r = r + l
                n = n - v
                break
    return r


test_cases = open(sys.argv[1], 'r')
for test in test_cases:
    if str.isspace(test):
        continue
    print(convertRoman(int(test)))

test_cases.close()
